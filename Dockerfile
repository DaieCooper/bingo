FROM ubuntu

WORKDIR /opt/bingo

COPY bingo /opt/bingo

RUN apt update && \
    apt upgrade -y && \
    apt install -y wget telnet curl tzdata && \
    apt autoremove -y && \
    adduser user --shell /bin/bash && \
    usermod -aG sudo user && \
    mkdir -p /opt/bongo/logs/d5cd37b3bd/ && \
    touch /opt/bongo/logs/d5cd37b3bd/main.log && \
    chmod 777 -R /opt/bongo/ && \
    chmod 777 -R /opt/bingo/

USER user

CMD ["./bingo", "run_server"]
