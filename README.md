# Развернуть отказоустойчивую инсталляцию приложения из имеющегося бинарника https://storage.yandexcloud.net/final-homework/bingo продукта. Планируется стабильная нагрузка в 60 RPS, пиковая в 120 RPS.
- Бинарник умеет в развернутой базе данных создавать необходимые для работы таблицы и наполнять их тестовыми данными. Нужно его только об этом правильно попросить.
- Важно убедиться в том, что на сервере настроено корректно точное время. Таймзона не имеет значения.
- Хэлсчек в приложении есть, живёт на GET /ping и свою функцию выполняет;
- Для самостоятельной проверки RPS и времени ответа своей инсталяции можете пользовать wrk / ab;
- Для SSL, http3 и кеширования мы рекомендуем использовать reverse proxy, которые это поддерживают, например nginx;
- Некоторый софт не умеет отдавать метрики в формате prometheus, для такого софта сбоку разворачивают что-то, что умеет собрать и отдать за него эти метрики. Гуглить по запросу "<название софта> export metrics".

# Требования (в порядке убывания важности):
- Отказоустойчивость: сервис должен быть развернут на двух нодах, отказ любой из них должен быть незаметен пользователю. Допускается просадка по RPS до стабильного значения в момент отказа любой из нод. При живости обеих нод, инсталяция обязана выдерживать пиковую нагрузку. Так же нужно обеспечить восстановление работоспособности любой отказавшей ноды быстрее, чем  за минуту.
- Сервис должен переживать пиковую нагрузку в 120 RPS в течение 1 минуты, стабильную в 60 RPS.
- Запросы POST /operation {"operation": <operation_id: integer>} должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат быстрее, чем за 400 миллисекунд в 90% случаев при 120 RPS, гарантируя не более 1% ошибок.
- Запросы GET /db_dummy должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат быстрее, чем за 400 миллисекунд в 90% случаев при 120 RPS, гарантируя не более 1% ошибок.
- Запросы GET /api/movie/{id} должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат быстрее, чем за 400 миллисекунд в 90%  случаев при 120 RPS, гарантируя не более 1% ошибок.
- Запросы GET /api/customer/{id} должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат быстрее, чем за 400 миллисекунд в 90% случаев при 120 RPS, гарантируя не более 1% ошибок.
- Запросы GET /api/session/{id} должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат быстрее, чем за 400 миллисекунд в 90%  случаев при 120 RPS, гарантируя не более 1% ошибок.
- Запросы GET /api/movie должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат гарантируя не более 1% ошибок. Требований по времени ответа нет, планируем делать не более одного такого запроса одновременно.
- Запросы GET /api/customer должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат гарантируя не более 1% ошибок. Требований по времени ответа нет, планируем делать не более одного такого запроса одновременно.
- Запросы GET /api/session должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат гарантируя не более 5% ошибок. Требований по времени  ответа нет, планируем делать не более одного такого запроса одновременно.
- Запросы POST /api/session должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат гарантируя не более 1% ошибок. Требований по  времени ответа и RPS нет.
- Запросы DELETE /api/session/{id} должны возвращать незакешированный ответ. Сервер должен обрабатывать такие запросы и отдавать результат гарантируя не более 1% ошибок. Требований по  времени ответа и RPS нет.
- Задача со звёздочкой: сделать так, чтобы сервис работал на отдельном домене по https протоколу, и по http без редиректа на https  (допускается самоподписанный сертификат).
- Задача со звёздочкой: сделать http3.
- Задача со звёздочкой: сделать так, чтобы запросы GET /long_dummy возвращали ответ не старше 1 минуты и отвечали быстрее, чем за 1 секунду в 75% случаев.
- Задача со звёздочкой: желательно обеспечить наблюдаемость приложения: графики RPS и ошибок по каждому эндпоинту.
- Задача со звёздочкой: автоматизировать развёртывание при помощи devops инструментов, с которыми вы успели познакомиться ранее.
# Решение
Бинарник приложения HTTP-запросы.
Вызываем команду help с бинарём:
```
./bingo --help
bingo

Usage:
   [flags]
   [command]

Available Commands:
  completion           Generate the autocompletion script for the specified shell
  help                 Help about any command
  prepare_db           prepare_db
  print_current_config print_current_config
  print_default_config print_default_config
  run_server           run_server
  version              version

Flags:
  -h, --help   help for this command

```
Далее заполняем конфиг своими данными:
```
student_email: sdobronravov@example.com
postgres_cluster:
  hosts:
  - address: localhost
    port: 5432
  user: postgres
  password: postgres
  db_name: postgres
  ssl_mode: disable
  use_closest_node: false

```
Далее поднимаем базу данных postgres в docker-compose.yml
```
---

version: "3"

services:
  postgres:
    image: postgres:14
    container_name: postgres
    deploy:
      resources:
        limits:
          cpus: '2'
          memory: "512mb"
        reservations:
          memory: "256mb"
    shm_size: "128mb"
    healthcheck:
      test: [ "CMD", "pg_isready", "-d", "postgres", "-U", "postgres" ]
      interval: "10s"
      timeout: "2s"
      retries: 3
      start_period: "15s"
    environment:
      - LANG=C.UTF-8
      - TZ=Europe/Moscow
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_USER=postgres
      - POSTGRES_DB=postgres
    volumes:
      - ./pgdata:/var/lib/postgresql/data:rw
    ports:
      - "127.0.0.1:5432:5432/tcp"
    logging: &logging
      driver: "json-file"
      options:
        max-size: "10m"
        max-file: "3"
    restart: always
```
При попытке вывести конфиг расположение конфига получаем ошибку. Начинаем стрейсить её:
```
strace -e file ./bingo print_current_config
execve("./bingo", ["./bingo", "print_current_config"], 0x7ffc5d6405b8 /* 49 vars */) = 0
openat(AT_FDCWD, "/sys/kernel/mm/transparent_hugepage/hpage_pmd_size", O_RDONLY) = 3
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=36075, si_uid=1000} ---
openat(AT_FDCWD, "/opt/bingo/config.yaml", O_RDONLY|O_CLOEXEC) = -1 ENOENT (Нет такого файла или каталога)
panic: failed to read config data

goroutine 1 [running]:
bingo/internal/config.mustGetSoftConfigFromFile({0xc0c485?, 0xc0000a2408?})
	/build/internal/config/soft.go:21 +0x3eb
bingo/internal/config.MustGetConfig()
	/build/internal/config/config.go:10 +0x73
main.addPrintCurrentConfigCommand.func1(0xc0000d4f00?, {0xba0387?, 0x4?, 0xba038b?})
	/build/cmd/bingo/print_current_config.go:16 +0x25
github.com/spf13/cobra.(*Command).execute(0xc0001faf00, {0x137d720, 0x0, 0x0})
	/home/vgbadaev/go/pkg/mod/github.com/spf13/cobra@v1.7.0/command.go:944 +0x863
github.com/spf13/cobra.(*Command).ExecuteC(0xc0001fa300)
	/home/vgbadaev/go/pkg/mod/github.com/spf13/cobra@v1.7.0/command.go:1068 +0x3a5
github.com/spf13/cobra.(*Command).Execute(...)
	/home/vgbadaev/go/pkg/mod/github.com/spf13/cobra@v1.7.0/command.go:992
main.main()
	/build/cmd/bingo/main.go:22 +0x85
+++ exited with 2 +++
```
Видим, что дефолтный путь конфига /opt/bingo/config.yaml. Учтём это

При попытке стартовать сервис, получаем ошибку, также стрейсим:
```
strace ./bingo run_server
```
И получаем дефолтный путь логов. Также учитываем
```
"/opt/bongo/logs/d5cd37b3bd/main.log"
```
